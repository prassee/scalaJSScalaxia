package com.scalaxia

import upickle._

case class Tweet(user: String, tweet: String)

case class Feed(name: String, foos: Seq[Tweet])

object JsonBackEnd {

  import com.scalaxia.HTTP._

  def readServerResponse(url: String = "") = read[Feed](get(url).value.get.get)

}
