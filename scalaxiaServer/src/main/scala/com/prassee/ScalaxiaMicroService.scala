package com.prassee

import java.util.concurrent.TimeUnit

import colossus._
import colossus.core.ServerSettings
import colossus.protocols.http.HttpMethod._
import colossus.protocols.http.UrlParsing._
import colossus.protocols.http._
import colossus.service._

import scala.concurrent.duration.Duration

/**
 * MicroService that helps to connect
 */
object ScalaxiaMicroService extends App {

  implicit val io_system = IOSystem()

  val responseTypes = List(("Content-Type", "application/json"))
  val x = Duration(10, TimeUnit.SECONDS)
  val sc = new ServiceConfig("ScalaxiaMicroService", x)

  Service.serve[Http](ServerSettings(10200), sc) {
    context =>
      import TwitterBackend._
      context.handle {
        connection =>
          connection.become {
            case request@Get on Root / "tweets" =>
              val twetJson = getTweetsFromTimeLine
              Callback.successful(request.ok( s"""${twetJson}""", responseTypes))
          }
      }
  }


}


