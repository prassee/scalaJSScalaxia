package com.scalaxia

import org.widok._
import org.widok.html._

/*
 * App
 */
object SinglePageApp extends PageApplication {

  import com.scalaxia.JsonBackEnd._

  private val tweets = Buffer[Ref[Tweet]]()

  private val handler = () => {
    readServerResponse("http://localhost:10200/tweets").foos.foreach { tweet =>
      println(tweet)
      tweets += Ref(tweet)
    }
  }

  def view() = div(
    h1("Scalaxia"),
    org.widok.html.button("Clear").onClick(c => tweets.clear())
    // ul(tweets.map { xt => li(xt.get.tweet) })
  )

  /*
   * called after the view is built
   * queries the remote service once for 20 secs
   * to update the UI
   */
  def ready() {
    org.scalajs.dom.setInterval(handler, 20000)
  }

}
