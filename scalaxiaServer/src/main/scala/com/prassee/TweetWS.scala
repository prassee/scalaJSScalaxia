package com.prassee

import java.util.TimerTask

import org.java_websocket.WebSocket
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer
import java.net.InetSocketAddress

class TweetWS(port: Int) extends WebSocketServer(new InetSocketAddress(port)) {

  private val connecitons = scala.collection.mutable.ListBuffer[WebSocket]()

  override def onError(webSocket: WebSocket, e: Exception): Unit = {}

  override def onMessage(webSocket: WebSocket, s: String): Unit = {
    connecitons.foreach(cxn => cxn.send(s))
  }

  override def onClose(webSocket: WebSocket, i: Int, s: String, b: Boolean): Unit =
    connecitons.-=(webSocket)

  override def onOpen(webSocket: WebSocket, clientHandshake: ClientHandshake): Unit =
    connecitons.+=:(webSocket)

  def publishData(s: String) =
    connecitons.foreach(cxn => cxn.send(s))

}

/**
 *
 */
object SocketServer extends App {
  val x = new TweetWS(10080)
  x.start()

  val tt = new TimerTask {
    override def run(): Unit = x.publishData("tweets arriaved")
  }

}
