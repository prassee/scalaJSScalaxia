import sbt._
import sbt.Keys._
import org.scalajs.sbtplugin._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._

object Build extends sbt.Build {
  val buildOrganisation = "com.scalaxia"
  val buildVersion = "0.1-SNAPSHOT"
  val buildScalaVersion = "2.11.7"
  val buildScalaOptions = Seq(
    "-unchecked", "-deprecation", "-encoding", "utf8", "-Xelide-below", annotation.elidable.ALL.toString
  )

  lazy val root = Project(id = "Root", base = file(".")).aggregate(main, colossus)

  lazy val main = Project(id = "scalaJSScalaxia", base = file("scalaJSScalaxia"))
    .enablePlugins(ScalaJSPlugin)
    .settings(
      libraryDependencies ++= Seq(
        "io.github.widok" %%% "widok" % "0.2.2",
        "com.lihaoyi" %%% "upickle" % "0.2.8",
        "com.lihaoyi" %% "utest" % "0.3.1"
      ),
      testFrameworks += new TestFramework("utest.runner.Framework"),
      organization := buildOrganisation,
      version := buildVersion,
      scalaVersion := buildScalaVersion,
      scalacOptions := buildScalaOptions,
      persistLauncher := true
    )


  lazy val colossus = Project(id = "scalaxiaServer", base = file("scalaxiaServer"))
    .settings(
      libraryDependencies ++= Seq(
        "org.scalatest" % "scalatest_2.11" % "2.2.2" % "test",
        "org.twitter4j" % "twitter4j-core" % "4.0.0",
        "org.java-websocket" % "Java-WebSocket" % "1.3.0",
        "com.tumblr" %% "colossus" % "0.6.1"
      ),
      organization := buildOrganisation,
      version := buildVersion,
      scalaVersion := buildScalaVersion
    )

}
